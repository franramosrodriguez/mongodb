# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 19:49:23 2018

@author: usuario
"""

import json
import re
from datetime import datetime

import pymongo
from pprint import pprint
from pymongo import MongoClient

# connect to database
connection = MongoClient('localhost', 27017)
db = connection.test

restaurants = db.restaurants

# handle to names collection


item = restaurants.find_one()

print(item['name'])

act = list(range(0,20))

# I limit the search in 10 querys for not overwarming.
#Query 1
item=restaurants.find().limit(10)
for doc in item:
   pprint(doc)
   
#Query 2
item=db.restaurants.find( {},{'name':1, 'borough':1, 'cuisine':1} ).limit(10)
for doc in item:
   pprint(doc) 

#Query 3
item=db.restaurants.find( {},{'name':1, 'borough':1, 'cuisine':1, '_id':0} ).limit(10)
for doc in item:
   pprint(doc)   
   
#Query 4
item=db.restaurants.find( { "borough": "Bronx" } ).limit(10)
for doc in item:
   pprint(doc)   

#Query 5
item=db.restaurants.find( { "borough": "Bronx" } ).limit(5)
for doc in item:
   pprint(doc)  

#Query 6
item=db.restaurants.find( { "borough": "Bronx" } ).limit(5).skip(5)
for doc in item:
   pprint(doc) 

#Query 7
item=db.restaurants.find(  { "grades.score": {"$gt": 90} }).limit(10)
for doc in item:
   pprint(doc) 

#Query 8
item=db.restaurants.find({ '$and':[{ "grades.score":{'$gt':80} }, { "grades.score": {'$lt':100}}]}).limit(10)
for doc in item:
   pprint(doc) 

#Query 9
item=db.restaurants.find(  { "address.coord.0": {'$lt': -95.754168}}).limit(10)
for doc in item:
   pprint(doc) 

#Query 10
item=db.restaurants.find(
	{ '$and':
		[
		{ "cuisine": {'$ne': "American" } },
		{ "grades.score": {'$gt': 70} },
		{"address.coord.0": {'$lt': -65.754168}}
		]
	}
).limit(10)

for doc in item:
   pprint(doc) 

#Query 11
item=db.restaurants.find({ '$and':[{ "cuisine": {'$ne': "American" } },{ '$and':[{ "grades.grade": "A" },{"borough": {'$ne': "Brooklyn" }}]}]}).sort("cuisine", pymongo.DESCENDING).limit(10)

for doc in item:
   pprint(doc) 

#Query 12
regx = re.compile("^Wil")
item=db.restaurants.find(	{ "name" : { '$regex': regx }	},	{"name":1, "borough":1, "cuisine":1} ).limit(10)

for doc in item:
   pprint(doc) 

#Query 13
regx = re.compile("ces$")
item=db.restaurants.find( { "name" : { '$regex': regx }},	{"name":1, "borough":1, "cuisine":1} ).limit(10)

for doc in item:
   pprint(doc) 

#Query 14
regx = re.compile(".*Reg.*")
item=db.restaurants.find({"name": regx},{"restaurant_id" : 1, "name":1,"borough":1, "cuisine" :1})

for doc in item:
   pprint(doc)
   
#Query 15
item=db.restaurants.find( { '$or':[{ "borough": "Queens" },{ "borough": "Staten Island" },{"borough": "Bronx"},{"borough": "Brooklyn"}]},{"name":1, "borough":1, "cuisine":1} ).limit(10)

for doc in item:
   pprint(doc)   

#Query 16
regx = re.compile("^Wil")
item=db.restaurants.find(
	{'$or': [
	  {'name': regx}, 
	  {"$and": [
		   {"cuisine" : {'$ne' :"American "}}, 
		   {"cuisine" : {'$ne' :"Chinees"}}
	   ]}
	]},
	{"name":1,"borough":1,"cuisine" :1}).limit(10)

for doc in item:
   pprint(doc)  

#Query 17
dt = datetime.strptime('2014-08-11','%Y-%m-%d')
item=db.restaurants.find( 	{ "grades.1.date": dt, "grades.1.grade":"A" , "grades.1.score" : 9}, {"restaurant_id" : 1,"name":1,"grades":1})

for doc in item:
   pprint(doc)  

#Query 18
item=db.restaurants.find().sort([("cuisine",1),("borough", -1)]).limit(10)

for doc in item:
   pprint(doc)

#Query 19
item=db.restaurants.find({"address.coord" : {'$type' : 1} }).limit(10)

for doc in item:
   pprint(doc)

#Query 20
item=db.restaurants.find(
	{"grades.score" :{'$mod' : [7,0]}},
		{"restaurant_id" : 1,"name":1,"grades":1}).limit(10)

for doc in item:
   pprint(doc)






